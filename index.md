---
layout: default
project: holoStuff™
---
{% assign pgw = 'http://127.0.0.1:8080' %}

# holoStuff™

* [web sites](sites.html), [sites.txt](sites.txt)
* [web pages](pages.html), [pages.txt](pages.txt)
* [*holo*stuff](stuff.html), [holo.txt](holo.txt)
* [web index](index.html), [added.log](added.log)
