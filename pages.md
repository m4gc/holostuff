---
layout: default
project: holoPages
---
# HoloPages ...
{% assign pgw = 'http://127.0.0.1:8080' %}
<!--
  0:mode 1:user.dev.ino 2:birth 3:a 4:m 5:ctime 6:size
  7:filepath 8:peerid 9:mac 10:fname 11:qm
-->

| name | file | mode | size | id5  | mac  |
| ---- | ---- | ---- | ---- | ---- | ---- |
{% for record in site.data.indexes %}{% assign i = forloop.index %}{% assign id5 = record[8] | slice: -5,5 %} | [{{record[10]| replace: '*','\*'}}]({{pgw}}/ipfs/{{record[11]}}) | [{{record[7] | prepend: '@' | replace: '/home/michelc/repo', id5 }}]({{pgw}}/ipfs/{{record[11]}}/index.html) | {{record[0]}} | {{record[6]}} | {{id5}} | {{record[9]}} |
{% endfor %}
