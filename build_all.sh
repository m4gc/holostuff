#

if false; then
locate -r /holo[A-Z][a-z]*$ | tee holo.txt
cat holo.txt | xargs -L 1 ipfs add --ignore-rules-path=.ipfsignore --pin=false -r | tee added.log
grep -v / added.log | uni9 | sed -e 's/added \(Q.*\) \(.*\)/- \2: \1/' > _data/stuff.yml

sed -i 's/^/- /' holo.txt > _data/holo.yml
fi


#locate -r /holo[A-Z]?[a-z]*/_site$ | tee _data/sites.txt
find ~/repo -name _site | tee sites.txt |\
xargs -L 1 sh bin/ipfs_add.sh -o _data/sites.yml~1
uni9 _data/sites.yml~1 > _data/sites.yml


# pages
cp -p _data/pages.yml _data/pages.yml~0
find ~/repo -name index.html | tee pages.txt |\
xargs -L 1 sh bin/ipfs_add.sh -o _data/pages.yml~1
uni9 _data/pages.yml~1 > _data/pages.yml


cd _includes
pandoc -f markdown -t html -o badges.html badges.md
cd ..
jekyll build -d _site
