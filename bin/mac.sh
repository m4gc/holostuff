#

ls -1d /sys/class/net/*/address | cut -d'/' -f 5 | tee if.txt
sed -e 's/^/- /' /sys/class/net/*/address | tee _data/mac.yml
cat /sys/class/net/*/address | paste _data/if.txt - | sed -e 's/\t/: /' > _data/if.yml

exit $?

true; # $Source: /my/shell/scripts/mac.sh $
