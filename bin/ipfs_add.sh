#

if [ "x$1" = 'x-o' ]; then
shift;
o="$1"
shift;
else
o=_data/ipfs_added.yml
fi

i="$1"
pwd=$(pwd)
if ! echo "$i" | grep -q '^/' ; then
  i="$pwd/$i"
fi
if [ -e _data/mac.yml ]; then
mac=$(tail -1 _data/mac.yml | cut -c 2- | tr -d :)
else
mac=000000000000
fi
peerid=$(ipfs --offline config Identity.PeerID)
f="${i%/index.html}"
p="${f%/_site}"
b="${p##*/}"
if [ "x$b" = 'x' ]; then b="$p"; fi
if [ "x$b" = 'x' ]; then b="$f"; fi
echo "f: $f"
echo "p: $p"
qm=$(ipfs add --ignore-rules-path=.ipfsignore -Q --pin=false -r "$f")
stat=$(stat -c '%a,%u.%D.%i, %W,%X,%Y,%Z, %s,%n' "$i")
echo "- [ $stat, $peerid,$mac, $b, $qm ]" >> "$o"
echo "$b: $qm"

exit $?;
true; # $Source: /my/shell/scripts/ipfs_add.sh $
