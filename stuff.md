---
layout: default
project: holoStuff™
---
## HoloStuff ...
{% assign pgw = 'http://127.0.0.1:8080' %}

{% for item in site.data.stuff %}{% for key in item %}
* [{{key[0]}}]({{pgw}}/ipfs/{{key[1]}}){% endfor %}{% endfor %}

